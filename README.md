# README #

Laravel (and Lumen) Systems Support 

### What is this repository for? ###

Combined laravel/lumen support libraries for common Suited Connector enterprise needs, 
including api traits, paladin helper, new relic reporting, and signup flow tracking.  

Combines and overrides the separate libraries.